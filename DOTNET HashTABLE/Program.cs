﻿//// HASH TABLE IN C#.

// SYNTAX

using DOTNET_HashTABLE;
using System.Collections;
/// How to create the hashtable.
/// 
Hashtable hashtable = new Hashtable();


//adding the element.
hashtable.Add(100, "ram");
hashtable.Add("area", "wifi");

// hashtable is similar to your dictionary and your sorted list but the main differeence of between
//them that is the hashcode because the HASHtable has generate the hashcode internally of your keys or values.


// second other things is that the hashtable can contains the the whole object of any class .


hashtable.Add(100, new student { Id = 200, Name = Program, age = 21 });
hashtable.Add(100, new student { Id = 200, Name = Program, age = 21 });

foreach (DictionaryEntry item in hashtable)
{
    Console.WriteLine($"key::{item.Key}\t value::{item.Value}");
}

Console.WriteLine("enter the key to be searched ");
int searchkey = int.Parse(Console.ReadLine());
if (hashtable.ContainsKey(searchkey)) ;
{
    Console.WriteLine(hashtable[searchkey]);
}
else
{
    Console.WriteLine("key not found");
}
    

    


