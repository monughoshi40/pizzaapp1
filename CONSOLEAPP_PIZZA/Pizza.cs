﻿internal class Pizza
{
    private int id;
    private string? name;
    private int price;

    public Pizza(int id, string? name, int price)
    {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}