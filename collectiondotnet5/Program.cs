﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");
Skip to content
GitLab
Toggle navigation Menu 
P
PizzaConsoleApp
Project information
Repository
Issues
0
Deployments
Packages and registries
Monitor
Analytics
Wiki
Snippets

Close sidebar
Open sidebar
Roshan Sahu
PizzaConsoleApp
master
pizzaconsoleapp
Program.cs
Program.cs
14.01 KiB
﻿using PizzaConsoleApp.Model;
using PizzaConsoleApp.Repository;
RepositoryProduct repositoryProductobj = new RepositoryProduct();
IRepositoryCustomer repositoryProductCustomer = (IRepositoryCustomer)repositoryProductobj;
IRepositoryPartner repositoryProductPartner = (IRepositoryPartner)repositoryProductobj;
Product[] allproduct;
int choice = 1;
while (choice != 0)
{
    Console.Clear();
    Console.WriteLine("Welcome To Pizza Conner");
    Console.WriteLine("1 : Customer Login");
    Console.WriteLine("2 : Partner Login");
    Console.WriteLine("3 : Exit");
    Console.Write("Enter your choice : ");
    choice = int.Parse(Console.ReadLine());
    //Costumer Login
    if (choice == 1)
    {
        bool flage = true;
        int choice1 = 1;
        allproduct = repositoryProductCustomer.GetAllProduct();
        while (choice1 != 0)
        {
            #region Display
            Console.Clear();
            Console.WriteLine("***************************");
            Console.WriteLine("\t\t\t\tIt's Pizza time");
            Console.WriteLine("***************************");
            Console.WriteLine("\t\tID\t\tPizzes\t\t\tPrice");
            Console.WriteLine("***************************");
            foreach (Product product in allproduct)
            {
                if (product == null) { break; }
                Console.WriteLine(product);
            }
            Console.WriteLine("***************************");
            Console.WriteLine();
            Console.WriteLine("1 : Sort pizza by names");
            Console.WriteLine("2 : Sort by price from low to high");
            Console.WriteLine("3 : Sort by price in from high to low");
            Console.WriteLine("4 : Order pizza");
            Console.WriteLine("5 : Cancel previous order");
            Console.WriteLine("6 : Exit");
            Console.Write("Enter your choice : ");
            choice1 = int.Parse(Console.ReadLine());
            #endregion
            switch (choice1)
            {
                case 1:
                    Console.Clear();
                    allproduct = repositoryProductCustomer.GetAllProduct();
                    break;
                case 2:
                    Console.Clear();
                    allproduct = repositoryProductCustomer.GetAllProductByAssending();
                    break;
                case 3:
                    Console.Clear();
                    allproduct = repositoryProductCustomer.GetAllProductByDesending();
                    break;
                case 4:
                    #region Order pizza
                    int choi = 1;
                    if (flage)
                    {
                        flage = false;

                    }
                    else
                    {
                        repositoryProductCustomer.CancelProduct();
                        flage = false;
                    }
                    do
                    {
                        Console.Clear();
                        Console.WriteLine("***************************");
                        Console.WriteLine("\t\t\t\tIt's Pizza time");
                        Console.WriteLine("***************************");
                        Console.WriteLine("\t\tID\t\tPizzes\t\t\tPrice");
                        Console.WriteLine("***************************");
                        foreach (Product product in allproduct)
                        {
                            if (product == null) { break; }
                            Console.WriteLine(product);
                        }
                        Console.WriteLine("***************************");
                        Console.WriteLine();
                        Console.Write("Enter pizza id to order pizza : ");
                        int productid = int.Parse(Console.ReadLine());
                        bool order_pizza_status = repositoryProductCustomer.OrderProduct(productid);
                        if (order_pizza_status)
                        {
                            Console.WriteLine("Pizza orderd successful");
                        }
                        else
                        {
                            Console.WriteLine("Invalid pizza id");
                        }
                        Console.WriteLine("1 : Order one more pizza");
                        Console.WriteLine("2 : Get bill");
                        Console.Write("Enter your choice : ");
                        choi = int.Parse(Console.ReadLine());
                        if (choi == 2)
                        {
                            choi = 0;
                            Console.WriteLine($"Total bill : {repositoryProductCustomer.GetBill()}");
                            Console.WriteLine("Enter any key to continue.");
                            Console.ReadLine();
                        }
                        else
                        {
                            choi = 1;
                        }
                    } while (choi != 0);
                    #endregion
                    break;
                case 5:
                    #region Cancel Pizza
                    Console.Clear();
                    bool cancel_pizza_status = repositoryProductCustomer.CancelProduct();
                    if (cancel_pizza_status)
                    {
                        Console.WriteLine("Order cancel successful");
                    }
                    else
                    {
                        Console.WriteLine("Order not found");
                    }
                    Console.Write("Enter any key to continue.");
                    Console.ReadLine();
                    Console.Clear();
                    #endregion
                    break;
                case 6:
                    Console.Clear();
                    choice1 = 0;
                    break;
                default:
                    Console.WriteLine("Enter correct choice");
                    Console.Write("Press enter to continue : ");
                    Console.ReadLine();
                    Console.Clear();
                    break;
            }
        }
    }
    //Partner Login
    else if (choice == 2)
    {
        Console.Clear();
        int choice2 = 1;
        while (choice2 != 0)
        {
            #region Display
            Console.Clear();
            Console.WriteLine("1 : List pizzas");
            Console.WriteLine("2 : Add a pizza");
            Console.WriteLine("3 : Delete a pizza");
            Console.WriteLine("4 : Update a pizza");
            Console.WriteLine("5 : Exit");
            Console.Write("Enter your choice : ");
            choice2 = int.Parse(Console.ReadLine());
            #endregion
            switch (choice2)
            {
                case 1:
                    #region List pizzas
                    Console.Clear();
                    allproduct = repositoryProductPartner.GetAllProduct();
                    Console.WriteLine("***************************");
                    Console.WriteLine("\t\t\t\tIt's Pizza time");
                    Console.WriteLine("***************************");
                    Console.WriteLine("\t\tID\t\tPizzes\t\t\tPrice");
                    Console.WriteLine("***************************");
                    foreach (Product product in allproduct)
                    {
                        if (product == null) { break; }
                        Console.WriteLine(product);
                    }
                    Console.WriteLine("***************************");
                    Console.WriteLine("Enter to continue.");
                    Console.ReadLine();
                    #endregion
                    break;
                case 2:
                    #region Add a pizza
                    Console.Clear();
                    Console.Write("Enter Pizza Id : ");
                    int id = int.Parse(Console.ReadLine());
                    Console.Write("Enter Pizza Name : ");
                    string name = Console.ReadLine();
                    Console.Write("Enter Pizza Price : ");
                    int price = int.Parse(Console.ReadLine());
                    Product add_product = new Product(id, name, price);
                    bool add_status = repositoryProductPartner.AddProduct(add_product);
                    if (add_status)
                    {
                        Console.WriteLine("Pizza added successful");
                    }
                    else
                    {
                        Console.WriteLine("Maximum limit of array is 20");
                    }
                    Console.Write("Enter any key to continue.");
                    Console.ReadLine();
                    #endregion
                    break;
                case 3:
                    #region Delete a pizza
                    Console.Clear();
                    Console.WriteLine("Delete a pizza");
                    Console.Write("Enter pizza id : ");
                    int del_product = int.Parse(Console.ReadLine());
                    bool del_status = repositoryProductPartner.DeleteProduct(del_product);
                    if (del_status)
                    {
                        Console.WriteLine("Pizza deleted successful");
                    }
                    else
                    {
                        Console.WriteLine("Invalid id");
                    }
                    Console.Write("Enter any key to continue.");
                    Console.ReadLine();
                    #endregion
                    break;
                case 4:
                    #region Update a pizza
                    Console.Clear();
                    Console.Write("Enter Pizza id : ");
                    int update_id = int.Parse(Console.ReadLine());
                    bool stu = repositoryProductPartner.GetIdValid(update_id);
                    if (stu == false)
                    {
                        Console.WriteLine("Invalid id");
                        Console.Write("Enter any key to continue.");
                        Console.ReadLine();
                        break;
                    }

                    int choi = 1;
                    while (choi != 0)
                    {
                        Console.Clear();
                        Console.WriteLine("**********");
                        Console.WriteLine($"Updating for id : {update_id}");
                        Console.WriteLine("**********");
                        Console.WriteLine("1 : Update pizza name");
                        Console.WriteLine("2 : Update price");
                        Console.WriteLine("3 : Exit");
                        Console.WriteLine("Enter your choice");
                        int c_choice = int.Parse(Console.ReadLine());
                        switch (c_choice)
                        {
                            case 1:
                                Console.Clear();
                                Console.WriteLine("**********");
                                Console.WriteLine($"Updating for id : {update_id}");
                                Console.WriteLine("**********");
                                Console.Write("Enter new name : ");
                                string update_name = Console.ReadLine();
                                repositoryProductPartner.UpdateProductName(update_id, update_name);
                                Console.WriteLine("Name updated successful");
                                Console.Write("Enter any key to continue.");
                                Console.ReadLine();
                                break;
                            case 2:
                                Console.Clear();
                                Console.WriteLine("**********");
                                Console.WriteLine($"Updating for id : {update_id}");
                                Console.WriteLine("**********");
                                Console.Write("Enter new price : ");
                                int update_price = int.Parse(Console.ReadLine());
                                repositoryProductPartner.UpdateProductPrice(update_id, update_price);
                                Console.WriteLine("Price updated successful");
                                Console.Write("Enter any key to continue.");
                                Console.ReadLine();
                                break;
                            case 3:
                                choi = 0;
                                break;
                            default:
                                Console.Write("Invalid input.");
                                break;
                        }
                    }
                    #endregion
                    break;
                case 5:
                    Console.Clear();
                    choice2 = 0;
                    break;
                default:
                    Console.WriteLine("Enter correct choice");
                    Console.Write("Press enter to continue : ");
                    Console.ReadLine();
                    Console.Clear();
                    break;
            }
        }
    }
    else if (choice == 3)
    {
        choice = 0;
    }
    else
    {
        Console.WriteLine("Enter correct choice");
        Console.Write("Press enter to continue : ");
        Console.ReadLine();
        Console.Clear();
    }
}